PowerAdmin - The Django Admin improved
======================================

Key features
--------------
* Multi search
* PowerButtons
* New permissions: '''has_view_permission''' and '''has_browse_permission'''
* Export CSV
* Report Generator


How to Install
--------------

1) Install from repository with PIP:

```bash
pip install git+https://bitbucket.org/josir/poweradmin.git/dev#egg=poweradmin

```

2) On `settings.py`, put `poweradmin` in your `INSTALLED_APPS` (before `admin`):

```
INSTALLED_APPS = (
    'poweradmin',
    'django.contrib.admin',
    ...
)
```

3) On your app `admin.py`, just alter one of the `ModelAdmin` definition to `PowerModelAdmin`

Example:


```
#!python

from django.contrib import admin
from poweradmin.admin import PowerModelAdmin
form myapp.models import MyModel

class MyModelAdmin(PowerModelAdmin):
    pass

admin.site.register(MyModel, MyModelAdmin)
```

How to use
--------------

1) Muli search, example:

```
#!python

from django.contrib import admin
from poweradmin.admin import PowerModelAdmin
form myapp.models import MyModel

class MyModelAdmin(PowerModelAdmin):
    multi_search = (
        ('q1', u'Label 1', ['field_name1']),
        ('q2', u'Label 2', ['field_name2']),
    )

admin.site.register(MyModel, MyModelAdmin)
```

2) PowerButton, example:

```
#!python

from django.contrib import admin
from django.core.urlresolvers import reverse
from poweradmin.admin import PowerModelAdmin
form myapp.models import MyModel

class MyModelAdmin(PowerModelAdmin):
    def get_buttons(self, request, object_id):
        buttons = super(MyModelAdmin, self).get_buttons(request, object_id)
        if object_id: # Button into change
            buttons.append(PowerButton(url=reverse('admin:myapp_mymodel_changelist'), label=u'Back'))
        else: # Button on list display
            buttons.append(PowerButton(url=u'http://google.com.br/', label=u'Google', attrs={'target': '_blank'}))
        return buttons

admin.site.register(MyModel, MyModelAdmin)
```

3) Export CSV, example:

```
#!python

from django.contrib import admin
from poweradmin.admin import PowerModelAdmin
form myapp.models import MyModel

class MyModelAdmin(PowerModelAdmin):
    list_csv = ('field_name1', 'field_name2', 'field_name3', )

admin.site.register(MyModel, MyModelAdmin)
```