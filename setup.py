# coding: utf-8
import re
import os
from setuptools import setup, find_packages


def read_file(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


def get_version():
    meta_filedata = read_file('poweradmin/__init__.py')
    re_version = re.compile(r'VERSION\s*=\s*\((.*?)\)')
    group = re_version.search(meta_filedata).group(1)
    version = filter(None, map(lambda s: s.strip(), group.split(',')))
    return '.'.join(version)


setup(
    name='poweradmin',
    version=get_version(),
    author='Vinicius Cainelli/Leonardo C. Santos',
    author_email='me@vinicius.ca/leonardocsantoss@gmail.com',
    description='An plug-and-play plugin for admin improvements',
    license='BSD',
    keywords='admin',
    url="https://bitbucket.org/viniciuscainelli/poweradmin/",
    packages=find_packages(),
    package_data  = { 
        'poweradmin': ['templates/admin/*'],
    },
    include_package_data=True,
    long_description=read_file('README.md'),
    classifiers=[
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
    ],
    install_requires=[r for r in read_file('requirements.txt').split('\n') if r or not r.strip().startswith('#')],
)
